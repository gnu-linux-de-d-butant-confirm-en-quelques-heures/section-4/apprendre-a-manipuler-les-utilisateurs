#Version 1.0

FROM ubuntu:22.10

RUN apt-get update && apt install man perl sudo nano vim -y && yes | unminimize

CMD ["sleep", "infinity"]