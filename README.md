# GNU/Linux de débutant à confirmé en quelques heures

Bonjour et bienvenue dans ce scénario créé par **Jordan ASSOULINE** pour vous aider à maîtriser les commandes permettant de manipuler les utilisateurs sur Linux.

Ainsi nous verrons en détail les commandes:
- `sudo`: permet d'exécuter des commandes en tant que certains utilisateurs
- `adduser`: permet de créer un nouvel utilisateur
- `deluser`: permet de supprimer un utilisateur

<br/>

# Introduction

Tout d'abord, je vous invite à vérifier que Docker est bien installé sur votre machine.
Pour cela tapez la commande :
`sudo apt update && sudo apt install docker docker.io -y`

Vous pouvez aussi utiliser le docker playground si vous ne souhaitez pas installer ces éléments sur votre propre machine :
https://labs.play-with-docker.com/

Pour lancer l'environnement de l'exercice, vous allez exécuter la commande :

`docker run -it --rm --hostname assouline jassouline/utilisateurs:latest bash`

Pour sortir de l'environnement, il suffit de taper la commande `exit` ou de taper sur la combinaison de touches : `CTRL + C`

<br/>

# Exercice 1

Dans ce premier exercice, nous allons utiliser la commande `adduser` pour créer de nouveaux utilisateurs.

Pour l'instant sur le système, il n'existe qu'un utilisateur **root** capable d'utiliser un shell.

1. Créez un nouvel utilisateur : **jordan** dont le mot de passe est : **jordan**

<details><summary> Montrer la solution </summary>
<p>
adduser jordan
</p>
</details>

Vous pouvez vérifier que le système a bien créé le dossier */home/jordan* grâce à la commande `ls -l /home/`


2. Maintenant, essayez de vous authentifier avec l'utilisateur **jordan**

<details><summary> Montrer la solution </summary>
<p>
su jordan
</p>
</details>

3. Une fois authentifier avec l'utilisateur **jordan**, essayez de créer un nouvel utilisateur **alexandre** grâce à la commande `adduser alexandre`

Cela ne fonctionne pas, car il est nécessaire d'avoir les droits root pour exécuter cette commande. 

4. Nous allons donc utiliser la commande : `sudo adduser alexandre`

Cela ne fonctionne toujours pas, car il est nécessaire d'appartenir aux "sudoers" pour pouvoir utiliser la commande `sudo`.

<br/>

# Exercice 2

Dans ce deuxième exercice, nous allons ajouter un utilisateur aux sudoers

Pour ajouter un utilisateur aux sudoers, il vous faut vous réauthentifier en utilisateur **root**.

Pour cela, vérifiez votre prompt : 
- Si votre prompt correspond à `jordan@assouline:/` alors utilisez la commande `exit`
- Si votre prompt correspond à `root@assouline:/` alors vous êtes déjà en root

1. Editez le fichier `/etc/sudoers` avec nano :

Observez bien le contenu du fichier jusqu'à trouver la ligne suivante :
```
# User privilege specification
root    ALL=(ALL:ALL) ALL
```

2. Juste en dessous de root, ajoutez la ligne 
`jordan    ALL=(ALL:ALL) ALL`, pour obtenir :
```
# User privilege specification
root    ALL=(ALL:ALL) ALL
jordan  ALL=(ALL:ALL) ALL
```

3. Sauvegardez et quittez le document.

4. Maintenant, une fois authentifier avec l'utilisateur **jordan** grâce à la commande `su jordan`, essayez de créer un nouvel utilisateur **alexandre** grâce à la commande `sudo adduser alexandre`

5. Spécifiez alors le mot de passe **alexandre** pour l'utilisateur **alexandre**.

<br/>

# Exercice 3

Dans ce dernier exercice, nous allons supprimer des utilisateurs.

Pour le moment, nous avons 3 utilisateurs sur notre système : jordan, alexandre et root. Pour rappel, jordan fait partie des **sudoers**.

1. Commencez par supprimer simplement l'utilisateur **alexandre**.

<details><summary> Montrer la solution </summary>
<p>
sudo deluser alexandre
</p>
</details>

2. Maintenant grâce à la commande `ls -l /home`, vérifiez si le répertoire de l'utilisateur alexandre a également été supprimé.

3. Supprimez le répertoire **/home/alexandre** grâce à la commande `rm`.

<details><summary> Montrer la solution </summary>
<p>
sudo rm -Rf /home/alexandre
</p>
</details>

4. Réauthentifiez vous en utilisateur **root** grâce à la commande `exit` si vous êtes toujours en **jordan**.

5. Supprimez l'utilisateur jordan, en intégrant automatiquement la suppression de son répertoire dans le /home grâce à l'option : `--remove-home`

<details><summary> Montrer la solution </summary>
<p>
sudo deluser --remove-home jordan
</p>
</details>

6. Vérifiez ensuite grâce à la commande `ls -l /home` que le répertoire de jordan a bien été supprimé grâce à la commande précédente.
